const { json } = require("express");
const express = require("express");
const app = express();
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");

app.use(express.json());
app.use(express.urlencoded({extended:true}))

const swaggerOptions = {
    swaggerDefinition:{
        info : {
            title: "Acamica API",
            version: "1.0.0"
        }
    },
    apis:["./swagger.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use("/api-docs",
        swaggerUI.serve,
        swaggerUI.setup(swaggerDocs));

// "BASE DE DATOS"
let users = [
    {
        id: 1,
        usuario: "sergiokcz",
        nombreApellido: "Sergio Karcz",
        email: "sergio@nada.com",
        telefono: "1111",
        direccion: "Av san martin 100",
        password: "1234",
        admin: true,
        logueado: false
    },
    {
        id: 2,
        usuario: "fabrigr",
        nombreApellido: "Fabricio grosso",
        email: "fabri@nada.com",
        telefono: "2222",
        direccion: "Av san martin 200",
        password: "4321",
        admin: false,
        logueado: false
    }
];

let productos = [
    {
        id: 1,
        nombre: "combo1",
        precio: 300,
    },
    {
        id: 2,
        nombre: "combo2",
        precio: 400
    }

];

let pedidos = [
    {
        pedidoID: 1,
        estado: 1,
        monto: 600,
        metodoDePago: 1,
        userID: 1,
        direccion: "Albatros 123",
        orden: [{ idProd: 1, cantidad: 2 }, { idProd: 2, cantidad: 2 }]
    }
];

let mediosDePago = [
    {
        codigo: 1,
        nombre: "Efectivo"
    },
    {
        codigo: 2,
        nombre: "Debito"
    },
    {
        codigo: 3,
        nombre: "Credito"
    },
    {
        codigo: 4,
        nombre: "Bitcoin"
    }
];

let estadoDePedido = [
    {
        codigo: 1,
        nombre: "Pendiente"
    },
    {
        codigo: 2,
        nombre: "Confirmado"
    },
    {
        codigo: 3,
        nombre: "En preparación"
    },
    {
        codigo: 4,
        nombre: "En camino"
    },
    {
        codigo: 5,
        nombre: "Entregado"
    }
]


        //MIDDLEWARES


function existeProd(req, res, next) {
    let prod = req.body;
    if (productos.find(function (producto) {
        return prod.idProd == producto.id;
    })
    ) next();
    else {
        res.send("Este producto no existe en la BDD");
    }
};

function existeUser(req, res, next) {
    let userID = req.params.userID;
    if (users.find(function (usuario) {
        return userID == usuario.id;
    })) {
        next();
    } else {
        res.send("El usuario no existe")
    };

}

function esAdmin(req, res, next) {
    let userID = req.params.userID;
    let usuarioAdmin=users.find(function(admin){
        return userID==admin.id;
    })
    if (usuarioAdmin.admin==true) {
        next();
    }
    else {
        res.send("El usuario no es administrador")
    }
};

function estaLogueado(req, res, next) {
    let userID = req.params.userID;
    let usuarioLogueado = users.find(function (usuario) {
        return userID == usuario.id;
    });
    if (usuarioLogueado.logueado == true) {
        next();
    } else {
        res.send("El usuario no ha iniciado sesión")
    };

};

function metodoValido(req, res, next) {
    let nuevoPedido = req.body;
    let validarMetodo = mediosDePago.findIndex(function (medio) {
        return nuevoPedido.metodoDePago == medio.codigo;
    })
    if (validarMetodo == -1) {
        res.send("El metodo de pago es invalido");
    } else {
        next();
    }
};


function validarProducto(req, res, next) {
    let validacion = true;
    let nuevaOrden = req.body;
    for (let i of nuevaOrden.orden) {
        if (productos.findIndex(function (producto) {
            return producto.id == i.idProd
        }) == -1) {
            validacion = false
            break;
        }
    }
    if (validacion == true) {
        next();
    } else {
        res.send("Algún elemento no fue encontrado")
    }
};

function direccionPedido(req, res, next) {
    let nuevoPedido = req.body;
    let userID = req.params.userID;
    if (nuevoPedido.direccion == "") {
        let usuarioEncontradisimo = users.find( function (usuario) {
            return userID == usuario.id;
        });
        if (usuarioEncontradisimo) {
            nuevoPedido.direccion = usuarioEncontradisimo.direccion;
            next();
        } else {
            res.send("La dirección del usuario no ha sido encontrada")
        };
    } else {
        next();
    }
};


function validarNuevoUsuario(req, res, next) {
    let newUser = req.body;
    if (newUser.usuario == "" || newUser.nombreApellido == "" || newUser.email == "" || newUser.telefono == "" || newUser.direccion == "" || newUser.password == "") {
        res.send("Todos los campos deben ser completados");
    }
    else {
        next();
    }
};

function validarMailDuplicado(req, res, next) {
    let newUser = req.body;
    if (users.find(function (user) {
        return user.email === newUser.email;
    })) {
        res.send("Este email se encuentra registrado")
    }
    else {
        next();
    }
};

function validarNombreDeUsuario(req, res, next) {
    let newUser = req.body;
    if (users.find(function (user) {
        return user.usuario == newUser.usuario;
    })) {
        res.send("Este nombre de usuario ya existe")
    }
    else {
        next();
    }
};


function estaAbierto(req, res, next) {
    let pedidoEdit = req.body;
    let modificable = pedidos.find(function (pedido) {
        return pedidoEdit.pedidoID == pedido.pedidoID;
    });

    if (modificable.estado == 1) {
        next();
    } else {
        res.send("Ya no es posible modificar su pedido")
    };

};




                    //ENDPOINTS

app.get("/pedidos/:userID", existeUser, estaLogueado, esAdmin, function (req, res) {
    res.status(201).send(pedidos);
});


app.post("/registro", validarNuevoUsuario, validarMailDuplicado, validarNombreDeUsuario, function (req, res) {
    let newUser = req.body;

    newUser.admin = false;
    newUser.logueado = false;
    newUser.id = users.length + 1;
    users.push(newUser);
    console.log(users);
    res.status(201).send("Usuario creado exitosamente")
});


app.post("/login", function (req, res) {
    let loginUsuario = req.body;
    var usuarioEncontrado = users.findIndex(function (user) {
        return (user.usuario == loginUsuario.usuario && user.password == loginUsuario.password);
    });
    if (usuarioEncontrado == -1) {
        res.send("El usuario o contraseña son incorrectos")
    } else {
        users[usuarioEncontrado].logueado = true;
        res.status(201).send("Sesión iniciada")
    }
});

app.post("/altaProducto/:userID", existeUser, estaLogueado, esAdmin, function (req, res) {
    let nuevoProducto = { id: req.body.id, precio: req.body.precio, nombre: req.body.nombre };
    productos.push(nuevoProducto);
    res.status(201).send("Producto añadido exitosamente");
});

app.delete("/borrarProducto/:userID", existeUser, estaLogueado, esAdmin, existeProd, function (req, res) {
    let producto = req.body.idProd;
    let nuevosProductos = productos.filter(productos => productos.id !== producto);
    productos = nuevosProductos;
    res.status(201).send("Producto eliminado exitosamente");

});


app.patch("/modificarProducto/:userID", existeUser, estaLogueado, esAdmin, existeProd, function (req, res) {
    let productoModif = req.body;

    let productoEncontrado2 = productos.findIndex(function (producto) {
        return productoModif.idProd == producto.id;
    });

    if (productoEncontrado2 != -1) {
        productos[productoEncontrado2].precio = productoModif.precio;
        productos[productoEncontrado2].nombre = productoModif.nombre;
        res.status(201).send("Producto modificado exitosamente");
    } else {
        res.send("El producto no existe en la BDD")
    }

});


app.post("/crearPedido/:userID", existeUser, estaLogueado, metodoValido, validarProducto, direccionPedido,function (req, res) {
    let nuevoPedido = {
        metodoPago: req.body.metodoDePago,
        direccion: req.body.direccion,
        orden: req.body.orden,
        monto: 0,
        estado: 1,
        pedidoID: pedidos.length + 1,
        userID: req.params.userID
    };
    for (let i of nuevoPedido.orden) {
        let productoEncontrado = productos.find(function (producto) {
            return i.idProd == producto.id;
        });
        nuevoPedido.monto += productoEncontrado.precio * i.cantidad;
    };
    pedidos.push(nuevoPedido);
    res.status(201).send("Pedido creado exitosamente")
});

app.get("/historial/:userID", existeUser, estaLogueado, function (req, res) {
    usuario = req.params;
    let historial = pedidos.filter(pedido => pedido.userID == usuario.userID);
    res.status(201).send(historial);
});

app.put("/modificarEstado/:userID", existeUser, estaLogueado, esAdmin, function (req, res) {
    nuevoEstado = req.body;
    if (estadoDePedido.find(function (estado) {
        return nuevoEstado.estado == estado.codigo;
    })) {
        let modificar = pedidos.findIndex(function (pedido) {
            return nuevoEstado.pedidoID == pedido.pedidoID;
        });
        if (modificar != -1) {
            pedidos[modificar].estado = nuevoEstado.estado;
            res.status(201).send("Estado de pedido modificado exitosamente");
        } else {
            res.send("El pedido no existe")
        }
    } else {
        res.send("El estado seleccionado no es valido");
    };
});


app.put("/crearMetodo/:userID", existeUser, estaLogueado, esAdmin, function (req, res) {
    nuevoMetodo = req.body;
    let validarCodigo = mediosDePago.findIndex(function (medio) {
        return nuevoMetodo.codigo == medio.codigo;
    });
    if (validarCodigo != -1) {
        res.send("El codigo para el nuevo metodo no es valido")
    } else {
        let metodo = { codigo: nuevoMetodo.codigo, nombre: nuevoMetodo.nombre }
        mediosDePago.push(metodo);
        res.status(201).send("Metodo de pago añadido exitosamente");
    }
});

app.patch("/modificarMetodo/:userID", existeUser, estaLogueado, esAdmin, function (req, res) {
    let metodoModif = req.body;

    let metodoEncontrado = mediosDePago.findIndex(function (medio) {
        return metodoModif.codigo == medio.codigo;
    });

    if (metodoEncontrado != -1) {
        mediosDePago[metodoEncontrado].nombre = metodoModif.nombre;
        mediosDePago[metodoEncontrado].codigo = metodoModif.codigo;
        res.status(201).send("Metodo de pago modificado exitosamente");
    } else {
        res.send("El metodo de pago indicado no existe")
    }

});

app.delete("/borrarMetodo/:userID", existeUser, estaLogueado, esAdmin, function (req, res) {
    let borrar = req.body;
    let nuevosMedios = mediosDePago.filter(medio => borrar.codigo != medio.codigo);
    if (nuevosMedios.length != 0) {
        mediosDePago = nuevosMedios;
        res.status(201).send("Metodo de pago eliminado");
    } else {
        res.send("El metodo indicado no existe");
    }
});

app.get("/mediosDePago/:userID", existeUser, estaLogueado, esAdmin, function (req, res) {
    res.status(201).send(mediosDePago);
});



app.patch("/modificarPedido/:userID", existeUser, estaLogueado, estaAbierto, validarProducto, function (req, res) {
    let nuevaOrden = req.body;
    nuevaOrden.userID=req.params.userID;
    let editar = pedidos.findIndex(function (pedido) {
        return nuevaOrden.pedidoID == pedido.pedidoID;
    });
    if (pedidos[editar].userID == nuevaOrden.userID) {
        pedidos[editar].orden = nuevaOrden.orden;
        pedidos[editar].monto = 0;
        for (let i of pedidos[editar].orden) {
            let productoEncontrado = productos.find(function (producto) {
                return i.idProd == producto.id;
            })
            pedidos[editar].monto += productoEncontrado.precio * i.cantidad;
        };
        res.status(201).send("Su orden fue modificada exitosamente");
    } else {
        res.send("Usted no puede modificar el pedido seleccionado");
    }
});



app.listen(3000, function () {
    console.log("Servidor corriendo sobre puerto 3000")
});

