# Primer Sprint Acamica

Primer sprint Acamica DWBE

Los archivos aquí disponibles son los necesarios para realizar el primer sprint de Desarrollo Web Back-End dictado por Acamica.

-INSTRUCCIONES DE INSTALACIÓN
    - Para la correcta ejecución del codigo deberá contar con NodeJS y su gestor de paquetes "npm".
    - Con npm, a través de la terminal deberá instalar express, swaggerJsDoc y swaggerUI a través del comando: npm i "nombre del paquete".
    - Una vez instalados, para levantar el servidor, es necesario tener disponible el puerto 3000 que viene configurado en el archivo o cambiar el mismo.
    - Finalmente, para acceder al swagger deberá hacerlo a traves de la URL: http://localhost:3000/api-docs


-sergiokcz
