//VER PEDIDOS
/**
 * @swagger
 * /pedidos/{userID}:
 *  get:
 *   description: Devuelve la lista de pedidos
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *   responses:
 *     201:
 *       pedidos
*/

//REGISTRAR NUEVO USUARIO
/**
 * @swagger
 * /registro:
 *  post:
 *   description: Crea un nuevo usuario
 *   parameters:
 *     - in: formData
 *       name: usuario
 *       schema:
 *       type: string
 *       required: true
 *       description: Nombre para el nuevo usuario
 *     - in: formData
 *       name: nombreApellido
 *       schema:
 *       type: string
 *       required: true
 *       description: Nombre y apellido del usuario
 *     - in: formData
 *       name: email
 *       schema:
 *       type: string
 *       required: true
 *       description: Email del nuevo usuario
 *     - in: formData
 *       name: telefono
 *       schema:
 *       type: string
 *       required: true
 *       description: Numero de telefono del nuevo usuario
 *     - in: formData
 *       name: direccion
 *       schema:
 *       type: string
 *       required: true
 *       description: Direccion del nuevo usuario
 *     - in: formData
 *       name: password
 *       schema:
 *       type: string
 *       required: true
 *       description: Contraseña del nuevo usuario
 *   responses:
 *     201:
 *       Usuario creado exitosamente
*/

//LOGIN DE USUARIO
/**
 * @swagger
 * /login:
 *  post:
 *   description: Cambia el estado de logueado a true
 *   parameters:
 *     - in: formData
 *       name: usuario
 *       schema:
 *       type: string
 *       required: true
 *       description: Nombre de usuario
 *     - in: formData
 *       name: password
 *       schema:
 *       type: string
 *       required: true
 *       description: Contraseña del usuario
 *   responses:
 *     201:
 *       Sesión iniciada
*/

//AÑADIR NUEVO PRODUCTO
/**
 * @swagger
 * /altaProducto/{userID}:
 *  post:
 *   description: Agrega un nuevo producto
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: formData
 *       name: id
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID del nuevo producto
 *     - in: formData
 *       name: nombre
 *       schema:
 *       type: string
 *       required: true
 *       description: Nombre del nuevo producto
 *     - in: formData
 *       name: precio
 *       schema:
 *       type: integer
 *       required: true
 *       description: Precio del nuevo producto
 *   responses:
 *     201:
 *       Producto añadido exitosamente
*/

//ELIMINAR UN PRODUCTO
/**
 * @swagger
 * /borrarProducto/{userID}:
 *  delete:
 *   description: Agrega un nuevo producto
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: formData
 *       name: idProd
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID del producto a eliminar
 *   responses:
 *     201:
 *       Producto eliminado exitosamente
*/

//MODIFICAR UN PRODUCTO
/**
 * @swagger
 * /modificarProducto/{userID}:
 *  patch:
 *   description: Modifica un producto
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: formData
 *       name: idProd
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID del producto a modificar
 *     - in: formData
 *       name: nombre
 *       schema:
 *       type: string
 *       required: true
 *       description: Nuevo nombre del producto
 *     - in: formData
 *       name: precio
 *       schema:
 *       type: integer
 *       required: true
 *       description: Nuevo precio del producto
 *   responses:
 *     201:
 *       Producto modificado exitosamente
*/

//CREAR NUEVO PEDIDO
/**
 * @swagger
 * /crearPedido/{userID}:
 *  post:
 *   description: Crea un nuevo pedido
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: body
 *       name: pedido
 *       description: Pedido del usuario
 *       schema:
 *         type: object
 *         required: 
 *           - Pedidos
 *         properties:
 *           orden:
 *             type: array
 *             items:
 *               type: object
 *               properties:
 *                 idProd:
 *                   type: integer
 *                 cantidad:
 *                   type: integer
 *           metodoDePago:
 *             type: integer
 *             required: true
 *           direccion:
 *             type: string
 *             required: false 
 *   responses:
 *     201:
 *       Pedido creado exitosamente
*/

//HISTORIAL
/**
 * @swagger
 * /historial/{userID}:
 *  get:
 *   description: Muestra el historial de pedidos del usuario
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *   responses:
 *     201:
 *       Historial 
*/

 //EDITAR ESTADO DE PEDIDO
/**
 * @swagger
 * /modificarEstado/{userID}:
 *  put:
 *   description: Modifica el estado de un pedido
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: formData
 *       name: pedidoID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID del pedido a modificar
 *     - in: formData
 *       name: estado
 *       schema:
 *       type: integer
 *       required: true
 *       description: Nuevo estado para el pedido
  *   responses:
 *     201:
 *       Estado de pedido modificado exitosamente
*/

//CREAR METODO DE PAGO
/**
 * @swagger
 * /crearMetodo/{userID}:
 *  put:
 *   description: Crea un nuevo metodo de pago
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: formData
 *       name: codigo
 *       schema:
 *       type: integer
 *       required: true
 *       description: Codigo del nuevo metodo de pago
 *     - in: formData
 *       name: nombre
 *       schema:
 *       type: string
 *       required: true
 *       description: Nombre del nuevo metodo de pago
 *   responses:
 *     201:
 *       Metodo de pago añadido exitosamente
*/

//EDITAR METODO DE PAGO
/**
 * @swagger
 * /modificarMetodo/{userID}:
 *  patch:
 *   description: Modifica un producto
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: formData
 *       name: codigo
 *       schema:
 *       type: integer
 *       required: true
 *       description: Codigo del metodo de pago a modificar
 *     - in: formData
 *       name: nombre
 *       schema:
 *       type: string
 *       required: true
 *       description: Nuevo nombre del metodo de pago
 *   responses:
 *     201:
 *       Metodo de pago modificado exitosamente
*/

//ELIMINAR METODO DE PAGO
/**
 * @swagger
 * /borrarMetodo/{userID}:
 *  delete:
 *   description: Modifica un producto
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: formData
 *       name: codigo
 *       schema:
 *       type: integer
 *       required: true
 *       description: Codigo del metodo de pago a eliminar
  *   responses:
 *     201:
 *       Metodo de pago eliminado exitosamente
*/

//VER TODOS LOS MEDIOS DE PAGO
/**
 * @swagger
 * /mediosDePago/{userID}:
 *  get:
 *   description: Trae listado de metodos de pago
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
  *   responses:
 *     201:
 *       Lista de metodos de pago
*/

//EDITAR PEDIDO MIENTRAS ESTE ABIERTO
/**
 * @swagger
 * /modificarPedido/{userID}:
 *  patch:
 *   description: Modifica un producto
 *   parameters:
 *     - in: path
 *       name: userID
 *       schema:
 *       type: integer
 *       required: true
 *       description: ID numerico del usuario
 *     - in: body
 *       name: pedido
 *       description: Pedido del usuario
 *       schema:
 *         type: object
 *         required: 
 *           - Pedidos
 *         properties:
 *           orden:
 *             type: array
 *             items:
 *               type: object
 *               properties:
 *                 idProd:
 *                   type: integer
 *                 cantidad:
 *                   type: integer
 *           pedidoID:
 *             type: integer
 *             required: true
 *   responses:
 *     201:
 *       Metodo de pago modificado exitosamente
*/



